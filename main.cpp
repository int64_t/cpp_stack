
#include "imports/stack.h"

#include <iostream>
#include <stack>
#include <cstring>

inline const char* bool_to_text(bool b) { return (b ? "True" : "False" ); }

std::string getUserInput(custom_data_strucuture::stack<char> &buff);
bool isPalindrome(custom_data_strucuture::stack<char> &buff);
bool endProgram(const std::string &buffer);

int main() {

  /* stack test

  custom_data_strucuture::stack<int> test, test2;

  test.push(10);
  test.push(20);
  test.push(30);

  std::cout << "Current size is : " << test.size() << std::endl;
  test2.recursiveAssignment(test);

  std::cout << "If the recursive assignment worked, test and test 2 are the same : "
  << bool_to_text(test == test2) << std::endl;

  for (int i = 0; i < 3 - 1; i++) {
    std::cout << i << ". Top value of the stack : " << test.top() << std::endl;
    test.pop();
  }

  std::cout << std::endl << "Is the stack empty? " << bool_to_text(test.empty()) << std::endl << std::endl;

  test2 = test; //assign operator

  std::cout << "Is test and test2 equal? " << bool_to_text(test == test2) << std::endl << std::endl;

  test.clear();
  std::cout << "If the clear worked, size should be 0 : " << test.size() << std::endl << std::endl;

   end of tests */

  custom_data_strucuture::stack<char> buffer;
  std::cout << "Please enter words or a sentence to see if it's a palindrome: " << std::endl << std::endl;

  while (true) {

    std::cout << "To evaluate : ";
    std::string tmp_buffer = getUserInput(buffer);

    if (endProgram(tmp_buffer))
      return 0;

    if (isPalindrome(buffer))
      std::cout << "It's a palindrome!" << std::endl;
    else
      std::cout << "It's not a palindrome :(" << std::endl;

    std::cout << std::endl;
  }

}

std::string getUserInput(custom_data_strucuture::stack<char> &buff) {

  std::string buffer;

  std::getline(std::cin, buffer);

  for (char i : buffer)
    if (isalpha(i))
      buff.push(isupper(i) ? static_cast<char&&>(tolower(i)) : i);

  return buffer;
}


bool isPalindrome(custom_data_strucuture::stack<char> &buff) {

  char tmp_buffer[buff.size()];
  unsigned long size = buff.size();
  unsigned long middle = size / 2;

  for (int i = 0; i < size; i++) {
    tmp_buffer[i] = buff.top();
    buff.pop();
  }

  for (int i = 0; i < size; i++)
    if (tmp_buffer[i] != tmp_buffer[(size -1) - i] && i != middle)
      return false;

  return true;
}

bool endProgram(const std::string& buffer) {

  return static_cast<bool>(strcmp(buffer.c_str(), "end") == 0);
}