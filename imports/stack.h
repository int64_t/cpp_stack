//
// Created by x on 10/29/18.
//

#ifndef LAB_5_STACK_H
#define LAB_5_STACK_H
#include <vector>
namespace custom_data_strucuture {
template<typename T>
class stack {
 private:
  struct cell { //this is a cell that holds a value and a pointer to the next one
    T element;
    cell *next_cell = nullptr;

    explicit cell(const T &e, cell *n) { //default constructor of cell
      this->element = e;
      this->next_cell = n;
    }
  };

  cell *top_cell = nullptr; //holds the address of the cell that's on top of the stack
  int stack_size = 0;       //holds the size of the stack (will always get it's value from the method size)

  void recursiveCopy(cell *current_cell);

 public:

  stack();                      //constructor
  ~stack();                     //destructor

  inline void push(const T& e);		    //add new element on the top
  inline void pop();					//remove the top element

  inline const T& top() const;			//returns the value of the first element
  inline T& top();				        //returns the value of the first element
  inline unsigned long size() const;	//returns the size

  inline bool empty() const;			//is stack empty
  inline void clear();				    //empty stack

  stack& operator=(const stack& s);	          //assignment operator
  bool operator==(const stack& s)const;	      //compares two stacks
  void recursiveAssignment(const stack& s);

};
}

template <typename T>
void custom_data_strucuture::stack<T>::recursiveCopy(custom_data_strucuture::stack<T>::cell *current_cell) {

  if (current_cell == nullptr)
    return;

  auto new_cell = new cell(current_cell->element, nullptr);
  new_cell->next_cell = this->top_cell;

  ++this->stack_size;
  this->recursiveCopy(current_cell->next_cell);
}

template<typename T>
inline custom_data_strucuture::stack<T>::stack() {

  this->stack_size = 0;
  this->top_cell = nullptr;
}


template <typename T>
inline  custom_data_strucuture::stack<T>::~stack() {

  cell *depth_ptr = this->top_cell;

  while (depth_ptr != nullptr) {

    cell *to_delete = depth_ptr;
    depth_ptr = depth_ptr->next_cell;

    delete to_delete;
  }

  this->stack_size = 0;
}

template <typename T>
inline unsigned long custom_data_strucuture::stack<T>::size() const {

  cell *depth_ptr = this->top_cell; //set our depth_ptr pointer to the first cell
  int size = 0;

  while (depth_ptr != nullptr) {

    ++size; //increase size by one
    depth_ptr = depth_ptr->next_cell; //go to the next cell
  }

  return size;
}

template <typename T>
inline const T& custom_data_strucuture::stack<T>::top() const {

  return static_cast<const T>(this->top_cell->element); //return a const version of the top cell
}

template <typename T>
inline T& custom_data_strucuture::stack<T>::top() {
  return this->top_cell->element; //return the top cell
}

template <typename T>
inline void custom_data_strucuture::stack<T>::push(const T &e) {

  auto new_top_cell = new cell(e, nullptr); //create a new cell
  new_top_cell->next_cell = this->top_cell; //point it's next cell to the current top cell

  this->top_cell = new_top_cell; //promote new cell to top cell
  ++this->stack_size; //increase size by one
}

template <typename T>
inline void custom_data_strucuture::stack<T>::pop() {

  if (this->top_cell != nullptr) {
    cell *new_top_cell = this->top_cell->next_cell; //get the address of the new top cell
    delete this->top_cell; //delete the old top cell

    this->top_cell = new_top_cell; //promote new_top_cell to top_cell
    --this->stack_size; //decrease size by one
  }
}

template <typename T>
inline bool custom_data_strucuture::stack<T>::empty() const {

  return this->top_cell == nullptr;
}

template <typename T>
inline void custom_data_strucuture::stack<T>::clear() {

  cell *impl_cell = this->top_cell;

  while(impl_cell != nullptr) {

    cell *to_delete = impl_cell;
    impl_cell = impl_cell->next_cell;

    delete to_delete;
  }
  this->top_cell = nullptr;
  this->stack_size = 0;
}

template <typename T>
custom_data_strucuture::stack<T>&
    custom_data_strucuture::stack<T>::operator=(const custom_data_strucuture::stack<T> &s) {

  if (s.size() > 0) {
    this->clear();
    cell *expl_cell = s.top_cell;

    for (int i = 0; i < s.size(); i++) {

      auto new_cell = new cell(expl_cell->element, nullptr);

      new_cell->next_cell = this->top_cell;
      this->top_cell = new_cell;
      expl_cell = expl_cell->next_cell;

      ++this->stack_size;
    }

  }
  return *this;
}

template <typename T>
bool custom_data_strucuture::stack<T>::operator==(const custom_data_strucuture::stack<T> &s) const {

  cell *impl_cell = this->top_cell;
  cell *expl_cell = s.top_cell;

  if (this->stack_size != s.size())
    return false;

  while (impl_cell != nullptr && expl_cell != nullptr) {

    if (impl_cell->element != expl_cell->element)
      return false;

    impl_cell = impl_cell->next_cell;
    expl_cell = expl_cell->next_cell;
  }

  return true;
}

template <typename T>
void custom_data_strucuture::stack<T>::recursiveAssignment(const custom_data_strucuture::stack<T> &s) {

  if (this->top_cell != nullptr)
    this->clear();

  this->recursiveCopy(s.top_cell);
}


#endif //LAB_5_STACK_H
